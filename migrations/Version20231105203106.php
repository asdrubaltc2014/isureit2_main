<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231105203106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
//        $this->addSql('CREATE TABLE customer_signature_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
//        $this->addSql('ALTER TABLE customer_siganture ADD status_id INT DEFAULT NULL');
//        $this->addSql('ALTER TABLE customer_siganture ADD CONSTRAINT FK_7673DC5F6BF700BD FOREIGN KEY (status_id) REFERENCES customer_signature_status (id)');
//        $this->addSql('CREATE INDEX IDX_7673DC5F6BF700BD ON customer_siganture (status_id)');
//        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
//        $this->addSql('ALTER TABLE customer_siganture DROP FOREIGN KEY FK_7673DC5F6BF700BD');
//        $this->addSql('DROP TABLE customer_signature_status');
//        $this->addSql('DROP INDEX IDX_7673DC5F6BF700BD ON customer_siganture');
//        $this->addSql('ALTER TABLE customer_siganture DROP status_id');
//        $this->addSql('ALTER TABLE `user` CHANGE roles roles JSON NOT NULL COMMENT \'(DC2Type:json)\'');
    }
}
