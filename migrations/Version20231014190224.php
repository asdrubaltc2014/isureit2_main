<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231014190224 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, state_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, date_of_birth VARCHAR(50) DEFAULT NULL, age INT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, suite VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, zip_code VARCHAR(10) DEFAULT NULL, family_size INT DEFAULT NULL, is_market_place TINYINT(1) DEFAULT NULL, current_house_hold DOUBLE PRECISION DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(25) DEFAULT NULL, most_recent_insurance_carrier VARCHAR(255) DEFAULT NULL, plan_name VARCHAR(255) DEFAULT NULL, current_monthly_premium DOUBLE PRECISION DEFAULT NULL, INDEX IDX_C74404556BF700BD (status_id), INDEX IDX_C74404555D83CC1 (state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notes (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, note LONGTEXT NOT NULL, INDEX IDX_11BA68C19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404556BF700BD FOREIGN KEY (status_id) REFERENCES client_status (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404555D83CC1 FOREIGN KEY (state_id) REFERENCES us_state (id)');
        $this->addSql('ALTER TABLE notes ADD CONSTRAINT FK_11BA68C19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404556BF700BD');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404555D83CC1');
        $this->addSql('ALTER TABLE notes DROP FOREIGN KEY FK_11BA68C19EB6921');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE notes');
    }
}
