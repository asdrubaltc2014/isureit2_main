<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231105204637 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD customer_signature_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455E3B9DB23 FOREIGN KEY (customer_signature_id) REFERENCES customer_siganture (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7440455E3B9DB23 ON client (customer_signature_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455E3B9DB23');
        $this->addSql('DROP INDEX UNIQ_C7440455E3B9DB23 ON client');
        $this->addSql('ALTER TABLE client DROP customer_signature_id');
    }
}
