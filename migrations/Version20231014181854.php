<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231014181854 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_siganture (id INT AUTO_INCREMENT NOT NULL, state_id INT DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, date_of_birth VARCHAR(100) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, suite VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(10) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(25) DEFAULT NULL, family_size INT DEFAULT NULL, is_info_above_correct TINYINT(1) DEFAULT NULL, is_marketplace TINYINT(1) DEFAULT NULL, current_household DOUBLE PRECISION DEFAULT NULL, expect_household DOUBLE PRECISION DEFAULT NULL, most_recent_carrier VARCHAR(255) DEFAULT NULL, plan_name VARCHAR(255) DEFAULT NULL, current_monthly_premium DOUBLE PRECISION DEFAULT NULL, is_insterested_renewing_current_plan TINYINT(1) DEFAULT NULL, signature VARCHAR(255) NOT NULL, signature_date DATETIME NOT NULL, has_code VARCHAR(255) NOT NULL, INDEX IDX_7673DC5F5D83CC1 (state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_siganture ADD CONSTRAINT FK_7673DC5F5D83CC1 FOREIGN KEY (state_id) REFERENCES us_state (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer_siganture DROP FOREIGN KEY FK_7673DC5F5D83CC1');
        $this->addSql('DROP TABLE client_status');
        $this->addSql('DROP TABLE customer_siganture');
    }
}
