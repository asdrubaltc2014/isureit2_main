<?php

namespace App\Services;

use App\Entity\CustomerSiganture;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GenerateSignatureReportService
{
    private $entityManager;
    private $conn;
    private $params;
    private $publicDirectory;
    private $dompdf;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->conn = $this->entityManager->getConnection();
        $this->params = $params;
        $this->publicDirectory = $this->params->get('kernel.project_dir');

        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isPhpEnabled', true);
        $this->dompdf = new Dompdf($options);
    }


    public function generatePdfFromHtml($htmlContent, $pdfFilename)
    {
        $this->dompdf->loadHtml($htmlContent);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();

        $publicDirectory = $this->publicDirectory;

        $folder = $publicDirectory.'/public/uploads/customerSignatures';

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $pdfFilename = $folder."/".$pdfFilename;
        $file = fopen($pdfFilename, 'w');
        fwrite($file, $output);
        fclose($file);
    }

    public function generateXLS(CustomerSiganture $customerSignature){


        return 'XLS';
    }


}