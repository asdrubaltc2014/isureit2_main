<?php

namespace App\Controller;

use App\Entity\ClientStatus;
use App\Form\ClientStatusType;
use App\Repository\ClientStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/configurations/client-status')]
class ClientStatusController extends AbstractController
{

    private $repository;
    private $em;

    /**
     * UserController constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em, ClientStatusRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    #[Route('/list', name: 'app_client_status_list')]
    public function index(): Response
    {

        $clientStatus = $this->repository->getAll();

        $actions=[
            [
                "id"=>0,
                "name"=>"delete",
            ],
        ];

        return $this->render('client_status/index.html.twig', [
            'actions' => $actions,
            'clientStatus' => $clientStatus,
        ]);
    }

    #[Route('/new', name: 'app_client_status_new', methods: ["GET","POST"])]
    public function add(Request $request): Response
    {
         $clientStatus = new ClientStatus();

         $form = $this->createForm(ClientStatusType::class, $clientStatus);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->repository->add($clientStatus, true);

            $this->addFlash(
                "success",
                "The Client Status has been created successfully!"
            );

            return $this->redirectToRoute('app_client_status_list');
        }

        return $this->render('client_status/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
    }

    #[Route('/update/{id}', name: 'app_client_status_update', defaults: ["id"=>null],  methods: ["GET","POST"])]
    public function update(Request $request, $id): Response
    {
        //$clientStatus = $this->repository->find($id);
        $clientStatus =new ClientStatus();

        $form = $this->createForm(ClientStatusType::class, $clientStatus);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->repository->add($clientStatus, true);

            $this->addFlash(
                "success",
                "The Client Status has been created successfully!"
            );

            return $this->redirectToRoute('app_client_status_list');
        }

        return $this->render('client_status/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
    }



}
