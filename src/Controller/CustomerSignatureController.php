<?php

namespace App\Controller;

use App\Entity\CustomerSiganture;
use App\Entity\CustomerSignatureStatus;
use App\Repository\CustomerSigantureRepository;
use App\Service\LogService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/customer-signature')]
class CustomerSignatureController extends AbstractController
{

    private $em;
    private $repository;
    private $log_service;

    public function __construct(EntityManagerInterface $em, CustomerSigantureRepository $repository,  LogService $log_service)
    {
        $this->em = $em;
        $this->repository = $repository;
        $this->log_service = $log_service;
    }

    #[Route('/list', name: 'customer_signature_list')]
    public function index(): Response
    {
        $prospects = $this->repository->getAll()->getResult();

        $actions=[
            [
                "id"=>0,
                "name"=>"Delete",
            ],
        ];


        $delete_form_ajax = $this->createCustomForm('CUSTOMER_ID', 'DELETE', 'delete_role');

        return $this->render('customer_signature/index.html.twig', [
            'prospects' => $prospects, "actions"=>$actions, 'delete_form_ajax' => $delete_form_ajax->createView()
        ]);
    }


    #[Route('/delete', name: 'delete_customer_signature', methods: ["POST", "DELETE"])]
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        $customer = $this->em->getRepository(CustomerSiganture::class)->find($id);
        $removed = 0;
        $message = "";

        if ($customer) {
            try {
                $this->em->remove($customer);
                $this->em->flush();
                $removed = 1;
                $message = "The Customer Signature has been Successfully removed";
            } catch (Exception $ex) {
                $removed = 0;
                $message = "The ustomer Signature can't be removed";
            }
        }

        return new Response(
            json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
        );
    }

    #[Route('/view-customer-signature/{id}', name: 'customer_signature_view', methods: ["GET"], defaults: ["id" => null])]
    public function viewCustomerSignature($id): Response
    {

        $object = $this->repository->find($id);

        return $this->render('customer_signature/view.html.twig',['action'=>'View','object'=>$object]);
    }

    #[Route('/change-status', name: 'customer_signature_change_status', methods: ["GET"])]
    public function changeStatus(Request $request): Response
    {

        $id = $request->query->get('id');
        $statusID = $request->query->get('statusId');

        $customer = $this->repository->find($id);
        $status = $this->em->getRepository(CustomerSignatureStatus::class)->find($statusID);

        if($customer != null and $status != null){
            $customer->setStatus($status);
            $this->em->flush();
        }

        return $this->redirectToRoute('customer_signature_list');
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
