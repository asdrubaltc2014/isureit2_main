<?php

namespace App\Controller;

use App\Entity\ClientStatus;
use App\Entity\Notes;
use App\Entity\UsState;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Service\LogService;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Client;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Ramsey\Uuid\Uuid;
use SendGrid\Mail\Mail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[Route('/clients')]
class ClientController extends AbstractController
{

    private $em;
    private $log_service;
    private $clientRepository;

    public function __construct(EntityManagerInterface $em, ClientRepository $clientRepository,  LogService $log_service)
    {
        $this->em = $em;
        $this->log_service = $log_service;
        $this->clientRepository = $clientRepository;
    }

    #[Route('/list', name: 'client_list', methods: ["GET"])]
    public function index(ClientRepository $clientRepository): Response
    {

        $clients = $clientRepository->getAll();

        $actions=[
            [
                "id"=>0,
                "name"=>"Delete",
            ],
            [
                "id"=>1,
                "name"=>"Signature Notification",
            ]
        ];

        return $this->render('client/list2.html.twig', [
            "actions" => $actions,
            "clients" => $clients
        ]);
    }

    #[Route('/upload-client', name: 'client_upload', methods: ["GET"])]
    public function uploadClient(): Response
    {

        return $this->render('client/upload-clients.html.twig',['action'=>'Upload']);
    }

    #[Route('/upload-client-process', name: 'client_upload_process', methods: ["POST"])]
    public function uploadClientProcess(Request $request): Response
    {
        $file = $request->files->get('file');

        if ($file) {
            $spreadsheet = IOFactory::load($file);
            $spreadsheet->setActiveSheetIndex(1);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $cont =1;
            foreach ($sheetData as $sheet) {
                if ($cont > 1) {
                    if ($sheet['A'] != null or $sheet['A'] != "") {

                        $hass = Uuid::uuid4();
                        $client =  new Client();
                        $status = $sheet['A'];

                        $statusObj = $this->em->getRepository(ClientStatus::class)->findOneBy(['name'=>$status]);

                        $client->setStatus($statusObj);
                        $client->setFirstName($sheet['B']);
                        $client->setLastName($sheet['C']);
                        $client->setDateOfBirth($sheet['D']);
                        $client->setAge(intval($sheet['E']));
                        $client->setAddress($sheet['F']);
                        $client->setSuite($sheet['G']);
                        $client->setCity($sheet['H']);
                        $client->setHass($hass);

                        $state = $sheet['I'];
                        $stateObj = $this->em->getRepository(UsState::class)->findOneBy(['abbreviation'=>$state]);

                        $client->setState($stateObj);
                        $client->setZipCode($sheet['J']);
                        $client->setFamilySize($sheet['K']);

                        $markedPlace = $sheet['L'];
                        if($markedPlace == 'Y'){
                            $client->setIsMarketPlace(true);
                        }else{
                            $client->setIsMarketPlace(false);
                        }

                        $currentHouseHold = $sheet['M'];

                        if($currentHouseHold != "N/A" and $currentHouseHold != ""){

                            $floatValue = (float)str_replace(['$', ',', 'USD'], '', $currentHouseHold);

                            $client->setCurrentHouseHold($floatValue);
                        }

                        $client->setEmail($sheet['N']);
                        $client->setPhone($sheet['O']);
                        $client->setMostRecentInsuranceCarrier($sheet['P']);
                        $client->setPlanName($sheet['Q']);
                        $currentMonthlyPremium = $sheet['R'];

                        if($currentMonthlyPremium != ""){
                            $floatValueMP = (float)str_replace(['$', ',', 'USD'], '', $currentMonthlyPremium);
                            $client->setCurrentMonthlyPremium($floatValueMP);
                        }

                        $this->em->persist($client);
                        $this->em->flush();

                        $note1 = $sheet['S'];
                        $note2 = $sheet['T'];
                        $note3 = $sheet['U'];
                        $note4 = $sheet['V'];

                        if($note1 != "" and $note1 != null){
                            $note = new Notes();
                            $note->setNote($note1);
                            $note->setClient($client);

                            $this->em->persist($note);
                            $this->em->flush();
                        }

                        if($note2 != "" and $note2 != null){
                            $note = new Notes();
                            $note->setNote($note2);
                            $note->setClient($client);

                            $this->em->persist($note);
                            $this->em->flush();
                        }

                        if($note3 != "" and $note3 != null){
                            $note = new Notes();
                            $note->setNote($note3);
                            $note->setClient($client);

                            $this->em->persist($note);
                            $this->em->flush();
                        }

                        if($note4 != "" and $note4 != null){
                            $note = new Notes();
                            $note->setNote($note4);
                            $note->setClient($client);

                            $this->em->persist($note);
                            $this->em->flush();
                        }
                    }
                }

                $cont++;
            }


        }

       return $this->redirectToRoute('client_list');
    }

    #[Route('/view-client/{id}', name: 'client_view',defaults: ['id' => null], methods: ["GET"])]
    public function viewClient($id): Response
    {

        $client = $this->clientRepository->find($id);

        return $this->render('client/view.html.twig',['action'=>'View','client'=>$client]);
    }

    #[Route('/create', name: 'client_create', methods: ["GET", "POST"])]
    public function create(Request $request): Response
    {
        $client = new Client();
        $form =  $this->createForm(ClientType::class, $client);

        return $this->render('client/add.html.twig', ['action' => 'New','form' => $form->createView(), 'client' => $client]);
    }

    #[Route('/update/{id}', name: 'client_update', defaults: ['id' => null], methods: ["GET", "POST"])]
    public function update(Request $request, $id): Response
    {

        $client = $this->em->getRepository(Client::class)->find($id);

        $form =  $this->createForm(ClientType::class, $client);

        return $this->render('client/add.html.twig', ['action' => 'Edit','form' => $form->createView(), 'client' => $client]);
    }

    #[Route('/send-notification', name: 'client_send_notification', methods: ["GET", "POST"])]
    public function sendNotification(Request $request, UrlGeneratorInterface $urlGenerator): Response
    {

        $sendGridTemplate = "d-66367a1fe75147c9b6661ad55ab0dd38";

        $date = date('m/d/Y');
        $ids = $request->get('ids');

        foreach ($ids as $id){

            $client = $this->em->getRepository(Client::class)->find($id);

            $client_email = $client->getEmail();
            $client_name = $client->getFirstName().' '.$client->getLastName();
            $hasCode = $client->getHass();

            $url_es = $urlGenerator->generate('app_public_customer_signature_request', [
                'hasToken' => $hasCode,
                'lng' => 'es'
            ]);

            $url_en = $urlGenerator->generate('app_public_customer_signature_request', [
                'hasToken' => $hasCode,
                'lng' => 'en'
            ]);

            $host = $request->getSchemeAndHttpHost();

            $url_es = $host.$url_es;
            $url_en = $host.$url_en;

            $email = new Mail();
            $email->setFrom('ek@Insuringsources.com');
            $email->addTo($client_email);
            $email->setTemplateId($sendGridTemplate);

            $data =[
                'date'=>$date,
                'client_name'=>$client_name,
                'url_es'=>$url_es,
                'url_en'=>$url_en,
            ];

            $email->addDynamicTemplateDatas($data);

            $sendgrid = new \SendGrid($_ENV['SENDGRID_API_KEY']);

            $response = $sendgrid->send($email);
            $body = $response->body();

            $sendGridResponse = $body;

            $status = $response->statusCode();
        }

        return new Response(
            json_encode(array('test' => $sendGridResponse)), 200, array('Content-Type' => 'application/json')
        );
    }


    #[Route('/delete', name: 'delete_client', methods: ["POST", "DELETE"])]
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        $client = $this->em->getRepository(Client::class)->find($id);
        $removed = 0;
        $message = "";

        if ($client) {
            try {

                //try to delete notes if exist
                $notes = $this->em->getRepository(Notes::class)->findBy(['client'=>$client->getId()]);
                if($notes){
                    foreach ($notes as $note){
                        $this->em->remove($note);
                        $this->em->flush();
                    }
                }


                $this->em->remove($client);
                $this->em->flush();
                $removed = 1;
                $message = "The Client has been Successfully removed";
            } catch (Exception $ex) {
                $removed = 0;
                $message = "The Client can't be removed";
            }
        }

        return new Response(
            json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
        );
    }


    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
