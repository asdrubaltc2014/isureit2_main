<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\CustomerSiganture;
use App\Entity\CustomerSignatureStatus;
use App\Form\CustomerSignatureType;
use App\Services\GenerateSignatureReportService;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController
{
    private $entityManager;

    /**
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    #[Route('/customer-signature-request', name: 'app_public_customer_signature_request',  methods: ["GET", "POST"])]
    public function index(Request $request, SessionInterface $session): Response
    {
        $hasCode = $request->query->get('hasToken');
        $language = $request->query->get('lng');
        $session->set('_locale', $language);

        $routeParams = ['hasCode' => $hasCode, 'lng' => $language];

        $status = $this->entityManager->getRepository(CustomerSignatureStatus::class)->find(1);

        $customer =  $this->entityManager->getRepository(CustomerSiganture::class)->findOneBy(
            ['hasCode'=>$hasCode]
        );

        if($customer != null){
            return $this->redirectToRoute('app_public_customer_signature_request_completed',$routeParams);
        }

        $customer = new CustomerSiganture();

        $is_marketPlace = 1;

        $client = null;
        if($hasCode != null){

            $client = $this->entityManager->getRepository(Client::class)->findOneBy(
                ['hass'=>$hasCode]
            );

            if($client != null){
                $customer->setFirstName($client->getFirstName());
                $customer->setLastName($client->getLastName());
                $customer->setDateOfBirth($client->getDateOfBirth());
                $customer->setAddress($client->getAddress());
                $customer->setSuite($client->getSuite());
                $customer->setCity($client->getCity());
                $customer->setState($client->getState());
                $customer->setZipcode($client->getZipCode());
                $customer->setEmail($client->getEmail());
                $customer->setPhone($client->getPhone());
                $customer->setFamilySize($client->getFamilySize());
                $customer->setIsMarketplace($client->isIsMarketPlace());
                $customer->setCurrentHousehold($client->getCurrentHouseHold());
                $customer->setMostRecentCarrier($client->getMostRecentInsuranceCarrier());
                $customer->setPlanName($client->getPlanName());
                $customer->setCurrentMonthlyPremium($client->getCurrentMonthlyPremium());
                $customer->setStatus($status);

                if($client->isIsMarketPlace()==true){
                    $is_marketPlace = 3;
                }else{
                    $is_marketPlace = 2;
                }
            }
        }

        if($hasCode == null){
            $hasCode = Uuid::uuid4();
        }

        $customer->setHasCode($hasCode);

        $form =  $this->createForm(CustomerSignatureType::class, $customer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $customer->setSignatureDate();
            $customer->setClient($client);

            $this->entityManager->persist($customer);

            $client->setCustomerSignature($customer);
            $this->entityManager->persist($client);

            $this->entityManager->flush();

            return $this->redirectToRoute('app_public_customer_signature_request_completed',$routeParams);
        }

        return $this->render('public/index.html.twig', ['customer' => $customer, 'client' => $client, 'form' => $form->createView(),'is_marketPlace'=>$is_marketPlace]);
    }

    #[Route('/customer-signature-request-completed', name: 'app_public_customer_signature_request_completed')]
    public function requestCompleted(Request $request, SessionInterface $session): Response
    {

        $language = $request->query->get('lng');
        $session->set('_locale', $language);

        $hasCode = $request->query->get('hasCode');

        return $this->render('public/register-completed.html.twig', ['hasCode'=>$hasCode, 'language'=>$language]);
    }


    #[Route('/downloadReport', name: 'app_public_customer_signature_download_report', methods: ["POST"])]
    public function downLoadReport(Request $request, GenerateSignatureReportService $reportService): Response
    {

        $language = $request->get('language');
        $hasCode = $request->get('hasCode');
        $type = $request->get('type');

        $publicDirectory = $this->getParameter('kernel.project_dir');

        $customerSignature = $this->entityManager->getRepository(CustomerSiganture::class)->findOneBy(
            ['hasCode'=>$hasCode]
        );

        if($type == 'pdf'){
            $pdfFilename = 'customer_'.$customerSignature->getHasCode().'_'.$customerSignature->getFullName(true).'.pdf';

            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $pdfOptions->set('isRemoteEnabled', TRUE);
            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView('pdf_templates/customerSignature.html.twig', [
                'customerSignature' => $customerSignature,
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();
            $output = $dompdf->output();


            $folder = $publicDirectory.'/public/concept-prospects/';
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $pdfFilepath =  $publicDirectory . '/public/concept-prospects/'.$pdfFilename;
            file_put_contents($pdfFilepath, $output);

            $response = new Response(file_get_contents($pdfFilepath));
            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'download; filename="' . $pdfFilename . '"');

            return $response;
        }

        if($type == 'xls'){
            $document = $reportService->generateXLS($customerSignature, $type);
        }

        return new Response('Downling File');
    }

    public function getHtmlContecttoPDF(CustomerSiganture $customerSignature, $language){

        $html = $this->renderView('pdf_templates/customerSignature.html.twig', [
            'customerSignature' => $customerSignature,
            'lng' => $language
        ]);

        return $html;
    }
}
