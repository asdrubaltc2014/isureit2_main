<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\CustomerSiganture;
use App\Repository\ClientRepository;
use App\Repository\CustomerRepository;
use App\Repository\CustomerSigantureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    private $entityManager;

    /**
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'dashboard')]
    public function index(CustomerSigantureRepository $customerRepository, ClientRepository $clientRepository): Response
    {

        $totalInProgress = $customerRepository->getTotalStatus(1);
        $completed = $customerRepository->getTotalStatus(2);
        $totalClient = $clientRepository->getTotal();
        $notYetStarted = $clientRepository->getNotYetStarted();

        return $this->render('dashboard/index.html.twig', [
            'totalCustomer' => $totalInProgress,
            'totalClient' => $totalClient,
            'notYetStarted' => $notYetStarted,
            'completed' => $completed
        ]);
    }
}
