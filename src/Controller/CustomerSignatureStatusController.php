<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomerSignatureStatusController extends AbstractController
{
    #[Route('/customer/signature/status', name: 'app_customer_signature_status')]
    public function index(): Response
    {
        return $this->render('customer_signature_status/index.html.twig', [
            'controller_name' => 'CustomerSignatureStatusController',
        ]);
    }
}
