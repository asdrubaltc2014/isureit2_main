<?php

namespace App\Command;

use App\Entity\Client;
use App\Entity\CustomerSiganture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'connect:client:customer',
    description: 'Add a short description for your command',
)]
class ConnectClientCustomerCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $clients = $this->entityManager->getRepository(Client::class)->findAll();

        $cont = 1;
        foreach ($clients as $client){
            $clientHass = $client->getHass();

            $customer = $this->entityManager->getRepository(CustomerSiganture::class)->findOneBy([
                'hasCode' => $clientHass
            ]);

            if($customer != null){
                $client->setCustomerSignature($customer);
            }

            $this->entityManager->persist($client);

            $io->writeln($cont.' '.$clientHass);
        }

        $this->entityManager->flush();

        $io->success('Process finished');

        return Command::SUCCESS;
    }
}
