<?php

namespace App\DataFixtures;

use App\Entity\ClientStatus;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UsState;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private  $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $state = new UsState();
        $state->setName('Florida');
        $state->setAbbreviation('FL');
        $manager->persist($state);

        $state = new UsState();
        $state->setName('Texas');
        $state->setAbbreviation('TX');
        $manager->persist($state);


        $manager->flush();

        $role = new Role();
        $role->setName('ROLE_ADMIN');
        $manager->persist($role);
        $manager->flush();

        $user = new User();
        $user->setFirstName('Asdrubal');
        $user->setLastName('Torres');
        $user->setEnabled(true);
        $user->setEmail('admin@isureit.com');

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            '314156aA@'
        );

        $user->setPassword($hashedPassword);
        $user->addUserRole($role);
        $manager->persist($user);
        $manager->flush();

        //Client Status
        $clientStatusData = [
            'Active 2023',
            'No Sale 2023',
            'Inactive'
        ];

        foreach ($clientStatusData as $data){
            $clientStatus = new ClientStatus();
            $clientStatus->setName($data);
            $manager->persist($clientStatus);
        }

        $manager->flush();
    }
}
