<?php

namespace App\Entity;

use App\Repository\CustomerSignatureStatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CustomerSignatureStatusRepository::class)]
class CustomerSignatureStatus
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'status', targetEntity: CustomerSiganture::class)]
    private Collection $customerSigantures;

    public function __construct()
    {
        $this->customerSigantures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, CustomerSiganture>
     */
    public function getCustomerSigantures(): Collection
    {
        return $this->customerSigantures;
    }

    public function addCustomerSiganture(CustomerSiganture $customerSiganture): static
    {
        if (!$this->customerSigantures->contains($customerSiganture)) {
            $this->customerSigantures->add($customerSiganture);
            $customerSiganture->setStatus($this);
        }

        return $this;
    }

    public function removeCustomerSiganture(CustomerSiganture $customerSiganture): static
    {
        if ($this->customerSigantures->removeElement($customerSiganture)) {
            // set the owning side to null (unless already changed)
            if ($customerSiganture->getStatus() === $this) {
                $customerSiganture->setStatus(null);
            }
        }

        return $this;
    }
}
