<?php

namespace App\Entity;

use App\Repository\CustomerSigantureRepository;
use App\Shared\BaseModel;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CustomerSigantureRepository::class)]

class CustomerSiganture extends BaseModel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    private ?string $firstName = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    private ?string $lastName = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Assert\NotBlank]
    private ?string $dateOfBirth = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $suite = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $zipcode = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Email]
    private ?string $email = null;

    #[ORM\Column(length: 25, nullable: true)]
    #[Assert\NotBlank]
    private ?string $phone = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    private ?int $familySize = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    private ?bool $isInfoAboveCorrect = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isMarketplace = null;

    #[ORM\Column(nullable: true)]
    private ?float $currentHousehold = null;

    #[ORM\Column(nullable: true)]
    private ?float $expectHousehold = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mostRecentCarrier = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $planName = null;

    #[ORM\Column(nullable: true)]
    private ?float $currentMonthlyPremium = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isInsterestedRenewingCurrentPlan = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message: 'The Signature should not be blank')]
    private ?string $signature = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $signatureDate = null;

    #[ORM\Column(length: 255)]
    private ?string $hasCode = null;

    #[ORM\ManyToOne(inversedBy: 'customer_signature')]
    private ?UsState $state = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(nullable: true)]
    private ?bool $needInsuranceNextYear = null;

    #[ORM\ManyToOne(inversedBy: 'customerSigantures')]
    private ?CustomerSignatureStatus $status = null;

    #[ORM\OneToOne(mappedBy: 'customerSignature', cascade: ['persist', 'remove'])]
    private ?Client $client = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSuite(): ?string
    {
        return $this->suite;
    }

    public function setSuite(?string $suite): self
    {
        $this->suite = $suite;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFamilySize(): ?int
    {
        return $this->familySize;
    }

    public function setFamilySize(?int $familySize): self
    {
        $this->familySize = $familySize;

        return $this;
    }

    public function isIsInfoAboveCorrect(): ?bool
    {
        return $this->isInfoAboveCorrect;
    }

    public function setIsInfoAboveCorrect(?bool $isInfoAboveCorrect): self
    {
        $this->isInfoAboveCorrect = $isInfoAboveCorrect;

        return $this;
    }

    public function isIsMarketplace(): ?bool
    {
        return $this->isMarketplace;
    }

    public function setIsMarketplace(?bool $isMarketplace): self
    {
        $this->isMarketplace = $isMarketplace;

        return $this;
    }

    public function getCurrentHousehold(): ?float
    {
        return $this->currentHousehold;
    }

    public function setCurrentHousehold(?float $currentHousehold): self
    {
        $this->currentHousehold = $currentHousehold;

        return $this;
    }

    public function getExpectHousehold(): ?float
    {
        return $this->expectHousehold;
    }

    public function setExpectHousehold(?float $expectHousehold): self
    {
        $this->expectHousehold = $expectHousehold;

        return $this;
    }

    public function getMostRecentCarrier(): ?string
    {
        return $this->mostRecentCarrier;
    }

    public function setMostRecentCarrier(?string $mostRecentCarrier): self
    {
        $this->mostRecentCarrier = $mostRecentCarrier;

        return $this;
    }

    public function getPlanName(): ?string
    {
        return $this->planName;
    }

    public function setPlanName(?string $planName): self
    {
        $this->planName = $planName;

        return $this;
    }

    public function getCurrentMonthlyPremium(): ?float
    {
        return $this->currentMonthlyPremium;
    }

    public function setCurrentMonthlyPremium(?float $currentMonthlyPremium): self
    {
        $this->currentMonthlyPremium = $currentMonthlyPremium;

        return $this;
    }

    public function isIsInsterestedRenewingCurrentPlan(): ?bool
    {
        return $this->isInsterestedRenewingCurrentPlan;
    }

    public function setIsInsterestedRenewingCurrentPlan(?bool $isInsterestedRenewingCurrentPlan): self
    {
        $this->isInsterestedRenewingCurrentPlan = $isInsterestedRenewingCurrentPlan;

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    public function setSignature(string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getSignatureDate(): ?\DateTimeInterface
    {
        return $this->signatureDate;
    }

    public function setSignatureDate()
    {
        $this->signatureDate = new \DateTime();

        return $this;
    }

    public function getHasCode(): ?string
    {
        return $this->hasCode;
    }

    public function setHasCode(string $hasCode): self
    {
        $this->hasCode = $hasCode;

        return $this;
    }

    /**
     * @return UsState|null
     */
    public function getState(): ?UsState
    {
        return $this->state;
    }

    /**
     * @param UsState|null $state
     */
    public function setState(?UsState $state): void
    {
        $this->state = $state;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function isNeedInsuranceNextYear(): ?bool
    {
        return $this->needInsuranceNextYear;
    }

    public function setNeedInsuranceNextYear(?bool $needInsuranceNextYear): static
    {
        $this->needInsuranceNextYear = $needInsuranceNextYear;

        return $this;
    }

    public function getFullName(?bool $uppercase = false): ?string {

        $fullName = $this->firstName." ".$this->lastName;
        if($uppercase == true){
            return strtoupper($fullName);
        }
        return $fullName;
    }

    public function getStatus(): ?CustomerSignatureStatus
    {
        return $this->status;
    }

    public function setStatus(?CustomerSignatureStatus $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): static
    {
        // unset the owning side of the relation if necessary
        if ($client === null && $this->client !== null) {
            $this->client->setCustomerSignature(null);
        }

        // set the owning side of the relation if necessary
        if ($client !== null && $client->getCustomerSignature() !== $this) {
            $client->setCustomerSignature($this);
        }

        $this->client = $client;

        return $this;
    }
}
