<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $lastName = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $dateOfBirth = null;

    #[ORM\Column(nullable: true)]
    private ?int $age = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $suite = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\ManyToOne(inversedBy: 'clients')]
    private ?ClientStatus $status = null;

    #[ORM\ManyToOne(inversedBy: 'clients')]
    private ?UsState $state = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $zipCode = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    private ?int $familySize = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isMarketPlace = null;

    #[ORM\Column(nullable: true)]
    private ?float $currentHouseHold = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mostRecentInsuranceCarrier = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $planName = null;

    #[ORM\Column(nullable: true)]
    private ?float $currentMonthlyPremium = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Notes::class)]
    private Collection $notes;

    #[ORM\Column(length: 255)]
    private ?string $hass = null;

    #[ORM\OneToOne(inversedBy: 'client', cascade: ['persist', 'remove'])]
    private ?CustomerSiganture $customerSignature = null;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSuite(): ?string
    {
        return $this->suite;
    }

    public function setSuite(?string $suite): self
    {
        $this->suite = $suite;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStatus(): ?ClientStatus
    {
        return $this->status;
    }

    public function setStatus(?ClientStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getState(): ?UsState
    {
        return $this->state;
    }

    public function setState(?UsState $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getFamilySize(): ?int
    {
        return $this->familySize;
    }

    public function setFamilySize(?int $familySize): self
    {
        $this->familySize = $familySize;

        return $this;
    }

    public function isIsMarketPlace(): ?bool
    {
        return $this->isMarketPlace;
    }

    public function setIsMarketPlace(?bool $isMarketPlace): self
    {
        $this->isMarketPlace = $isMarketPlace;

        return $this;
    }

    public function getCurrentHouseHold(): ?float
    {
        return $this->currentHouseHold;
    }

    public function setCurrentHouseHold(?float $currentHouseHold): self
    {
        $this->currentHouseHold = $currentHouseHold;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMostRecentInsuranceCarrier(): ?string
    {
        return $this->mostRecentInsuranceCarrier;
    }

    public function setMostRecentInsuranceCarrier(?string $mostRecentInsuranceCarrier): self
    {
        $this->mostRecentInsuranceCarrier = $mostRecentInsuranceCarrier;

        return $this;
    }

    public function getPlanName(): ?string
    {
        return $this->planName;
    }

    public function setPlanName(?string $planName): self
    {
        $this->planName = $planName;

        return $this;
    }

    public function getCurrentMonthlyPremium(): ?float
    {
        return $this->currentMonthlyPremium;
    }

    public function setCurrentMonthlyPremium(?float $currentMonthlyPremium): self
    {
        $this->currentMonthlyPremium = $currentMonthlyPremium;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setClient($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getClient() === $this) {
                $note->setClient(null);
            }
        }

        return $this;
    }

    public function getHass(): ?string
    {
        return $this->hass;
    }

    public function setHass(string $hass): self
    {
        $this->hass = $hass;

        return $this;
    }

    public function getCustomerSignature(): ?CustomerSiganture
    {
        return $this->customerSignature;
    }

    public function setCustomerSignature(?CustomerSiganture $customerSignature): static
    {
        $this->customerSignature = $customerSignature;

        return $this;
    }

}
