<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Client>
 *
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function add(Client $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Client $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getTotal(){
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getNotYetStarted(){

        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->LeftJoin('u.customerSignature','c')
            ->where('c IS NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAll(){

        $query = $this->createQueryBuilder('u')
            ->select('u.id','u.email','u.firstName','u.lastName','s.name as status', 'u.phone','u.hass','c.id as customerID')
            ->leftJoin('u.status', 's')
            ->leftJoin('u.customerSignature', 'c')
            ->getQuery()->getResult();

        return  $query;
    }

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->select('u.id','u.email','u.firstName','u.lastName')
            ->where('u.id = :id')
            ->orwhere('u.email LIKE :email')
            ->orwhere('u.firstName LIKE :firstName')
            ->orwhere('u.lastName LIKE :lastName')
            ->setParameter('id', $value)
            ->setParameter('email', '%'.$value.'%')
            ->setParameter('firstName', '%'.$value.'%')
            ->setParameter('lastName', '%'.$value.'%')
            ->getQuery()
            ;
    }

}
