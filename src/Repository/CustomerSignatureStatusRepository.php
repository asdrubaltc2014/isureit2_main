<?php

namespace App\Repository;

use App\Entity\CustomerSignatureStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerSignatureStatus>
 *
 * @method CustomerSignatureStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerSignatureStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerSignatureStatus[]    findAll()
 * @method CustomerSignatureStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerSignatureStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerSignatureStatus::class);
    }

//    /**
//     * @return CustomerSignatureStatus[] Returns an array of CustomerSignatureStatus objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CustomerSignatureStatus
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
