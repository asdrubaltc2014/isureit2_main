<?php

namespace App\Repository;

use App\Entity\CustomerSiganture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CustomerSiganture>
 *
 * @method CustomerSiganture|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerSiganture|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerSiganture[]    findAll()
 * @method CustomerSiganture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerSigantureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerSiganture::class);
    }

    public function add(CustomerSiganture $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CustomerSiganture $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getTotal(){
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getTotalStatus($status){
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.status = :status ')
            ->setParameter('status', $status)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAll(){

        return $this->createQueryBuilder('u')
            ->select('u.id','u.email','u.firstName','u.lastName', 'u.phone','u.hasCode','s.id as statusId', 's.name as statusName')
            ->leftJoin('u.status', 's')
            ->getQuery()
            ;
    }

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->select('u.id','u.email','u.firstName','u.lastName', 'u.hasCode','s.id as statusId', 's.name as statusName')
            ->leftJoin('u.status', 's')
            ->where('u.id = :id')
            ->orwhere('u.email LIKE :email')
            ->orwhere('u.firstName LIKE :firstName')
            ->orwhere('u.lastName LIKE :lastName')
            ->setParameter('id', $value)
            ->setParameter('email', '%'.$value.'%')
            ->setParameter('firstName', '%'.$value.'%')
            ->setParameter('lastName', '%'.$value.'%')
            ->getQuery()
            ;
    }


}
