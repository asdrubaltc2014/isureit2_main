<?php

namespace App\Repository;

use App\Entity\ClientStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClientStatus>
 *
 * @method ClientStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientStatus[]    findAll()
 * @method ClientStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientStatus::class);
    }

    public function add(ClientStatus $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ClientStatus $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getTotal(){
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAll(){

        $query = $this->createQueryBuilder('u')
            ->select('u.id','u.name','u.description')
            ->getQuery()->getResult();

        return  $query;
    }

//    /**
//     * @return ClientStatus[] Returns an array of ClientStatus objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ClientStatus
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
