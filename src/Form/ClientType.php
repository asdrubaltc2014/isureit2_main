<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\ClientStatus;
use App\Entity\UsState;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, ['label'=>'First Name: (*)','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('lastName', TextType::class, ['label'=>'Last Name: (*)','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('dateOfBirth', TextType::class,
                ['label'=>'Date of Birth:',
                    'attr' => ['placeholder' => 'dd/mm/yyyy','data-inputmask' => "'mask': '99/99/9999'"],
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']
                ])
            ->add('age', IntegerType::class, ['label'=>'Age:','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('address',TextType::class, ['label'=>'Address:','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('suite', TextType::class, ['label'=>'Suite Number:','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('city',TextType::class, ['label'=>'sign_form.label.city','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('state', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => UsState::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                }, 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                    'attr'=>['class'=>'form-select', 'data-control'=>'select2'] ,'label'=>'sign_form.label.state',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark fs-6 mb-2 mt-5']]
            )
            ->add('zipCode', TextType::class,
                [
                    'label'=>'Zip Code:',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5'],
                    'attr' => ['data-inputmask' => "'mask': '99999'"]
                ])
            ->add('email', TextType::class,
                ['label'=>'sign_form.label.email',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5'],
                    'attr' => ['data-inputmask' => "'alias': 'email'"]
                ])
            ->add('phone', TextType::class, [
                'label'=>'sign_form.label.phone',
                'attr' => ['data-inputmask' => "'mask': '999-999-9999'"],
                'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('familySize', IntegerType::class, ['label'=>'sign_form.label.family_size','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('isMarketplace', ChoiceType::class,
                [
                    'attr'=>['class'=>'form-check-inline mb-5 form-check-sm'],
                    'label'=>'sign_form.label.marketplace',
                    'label_attr'=>['class'=>'text-dark mb-2 fw-bold pt-2'],
                    'choices'=>[
                        'Yes' => true,
                        'No' => false
                    ],
                    'expanded'=>true, 'multiple' => false
                ])
            ->add('currentHouseHold', TextType::class, ['label'=>'sign_form.label.currentHousehold','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('mostRecentInsuranceCarrier', TextType::class, ['label'=>'sign_form.label.recent_carrier','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('planName', TextType::class, ['label'=>'sign_form.label.planName','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('currentMonthlyPremium', TextType::class, ['label'=>'sign_form.label.monthly_premium','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('status', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => ClientStatus::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                }, 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                    'attr'=>['class'=>'form-select', 'data-control'=>'select2'] ,'label'=>'Status',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark fs-6 mb-2 mt-5']]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
