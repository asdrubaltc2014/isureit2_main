<?php

namespace App\Form;

use App\Entity\CustomerSiganture;
use App\Entity\UsState;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerSignatureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, ['label'=>'sign_form.label.first_name','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('lastName', TextType::class, ['label'=>'sign_form.label.last_name','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('dateOfBirth', TextType::class,
                ['label'=>'sign_form.label.dob',
                    'attr' => ['placeholder' => 'dd/mm/yyyy','data-inputmask' => "'mask': '99/99/9999'"],
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']
                ])
            ->add('address', TextType::class, ['label'=>'sign_form.label.address','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('suite', TextType::class, ['label'=>'sign_form.label.suite_number','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('city', TextType::class, ['label'=>'sign_form.label.city','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('state', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => UsState::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                }, 'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                    'attr'=>['class'=>'form-select', 'data-control'=>'select2'] ,'label'=>'sign_form.label.state',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark fs-6 mb-2 mt-5']]
            )
            ->add('zipcode', TextType::class,
                [
                    'label'=>'sign_form.label.zip_code',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5'],
                    'attr' => ['data-inputmask' => "'mask': '99999'"]
                ])
            ->add('email', TextType::class,
                ['label'=>'sign_form.label.email',
                    'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5'],
                    'attr' => ['data-inputmask' => "'alias': 'email'"]
                ])
            ->add('phone', TextType::class, [
                'label'=>'sign_form.label.phone',
                'attr' => ['data-inputmask' => "'mask': '999-999-9999'"],
                'label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('familySize', IntegerType::class, ['label'=>'sign_form.label.family_size','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('isInfoAboveCorrect', ChoiceType::class,
                [
                    'attr'=>['class'=>'form-check-inline mb-5 form-check-sm'],
                    'label'=>'sign_form.label.is_info_correct',
                    'label_attr'=>['class'=>'text-dark mb-2 fw-bold pt-2'],
                    'choices'=>[
                        'Yes' => true,
                        'No' => false
                    ],
                    'expanded'=>true, 'multiple' => false
                ])
            ->add('isMarketplace', ChoiceType::class,
                [
                    'attr'=>['class'=>'form-check-inline mb-5 form-check-sm'],
                    'label'=>'sign_form.label.marketplace',
                    'label_attr'=>['class'=>'text-dark mb-2 fw-bold pt-2'],
                    'choices'=>[
                        'Yes' => true,
                        'No' => false
                    ],
                    'expanded'=>true, 'multiple' => false
                ])
            ->add('currentHousehold', TextType::class, ['label'=>'sign_form.label.currentHousehold','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('expectHousehold', TextType::class, ['label'=>'sign_form.label.expectHousehold','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('mostRecentCarrier', TextType::class, ['label'=>'sign_form.label.recent_carrier','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('planName', TextType::class, ['label'=>'sign_form.label.planName','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])

            ->add('currentMonthlyPremium', TextType::class, ['label'=>'sign_form.label.monthly_premium','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('isInsterestedRenewingCurrentPlan', ChoiceType::class,
                [
                    'attr'=>['class'=>'form-check-inline mb-5 form-check-sm'],
                    'label'=>'sign_form.label.interested_renew',
                    'label_attr'=>['class'=>'text-dark mb-2 fw-bold pt-2'],
                    'choices'=>[
                        'Yes' => true,
                        'No' => false
                    ],
                    'expanded'=>true, 'multiple' => false
                ])
            ->add('needInsuranceNextYear', ChoiceType::class,
                [
                    'attr'=>['class'=>'form-check-inline mb-5 form-check-sm'],
                    'label'=>'sign_form.label.needInsuranceNextYear',
                    'label_attr'=>['class'=>'text-dark mb-2 fw-bold pt-2'],
                    'choices'=>[
                        'Yes' => true,
                        'No' => false
                    ],
                    'expanded'=>true, 'multiple' => false
                ])
            ->add('signature', TextType::class, ['label'=>'sign_form.label.siganture','label_attr'=>['class'=>'form-label fw-bold text-dark mb-2 mt-5']])
            ->add('signatureDate', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CustomerSiganture::class,
        ]);
    }
}
