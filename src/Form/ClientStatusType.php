<?php

namespace App\Form;

use App\Entity\ClientStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'form-label text-primary fw-bolder text-dark fs-6 mb-2 mt-5']])
            ->add('description', TextareaType::class, ['label'=>'Description:','label_attr'=>['class'=>'form-label text-primary fw-bolder text-dark fs-6 mb-2 mt-5']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientStatus::class,
        ]);
    }
}
